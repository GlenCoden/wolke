const nodemailer = require('nodemailer');


function sendMail({ from, to, subject, text, html }) {
    return new Promise((resolve, reject) => {
        const auth = {};

        try {
            auth.SMTP_USER = process.env.SMTP_USER;
            auth.SMTP_PASS = process.env.SMTP_PASS;
        } catch (err) {
            console.error(err);
            reject(err);
        }

        const transporter = nodemailer.createTransport({
            host: 'smtp.1und1.de',
            port: 587,
            secure: false,
            auth: {
                user: auth.SMTP_USER,
                pass: auth.SMTP_PASS
            }
        });

        const options = { from, to, subject };

        if (text) {
            options.text = text;
        }

        if (html) {
            options.html = html;
        }

        transporter.sendMail(options, (err, info) => {
            if (err) {
                console.error(err);
                reject(err);
            } else {
                resolve({
                    messageId: info.messageId
                });
            }
        });
    });
}

module.exports = sendMail;