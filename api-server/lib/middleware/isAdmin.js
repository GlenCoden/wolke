function isAdmin(req, res, next) {
    if (req.body.admin_password !== process.env.ADMIN_PASSWORD) {
        res.status(403).json({
            success: false,
            message: 'wrong admin password'
        });
        return;
    }
    next();
}

module.exports = {
    isAdmin
};