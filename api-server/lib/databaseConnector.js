const Sequelize = require('sequelize');

const USER = 'glencoden';
const PASSWORD = 'example';

const InitType = {
    PERSIST: 'persist',
    UPDATE: 'update',
    RESET: 'reset'
};

const initOptions = {
    [InitType.PERSIST]: {}, // creates the table if it doesn't exist (and does nothing if it already exists)
    [InitType.UPDATE]: { alter: true }, // checks what is the current state of the table in the database (which columns it has, what are their data types, etc), and then performs the necessary changes in the table to make it match the model
    [InitType.RESET]: { force: true } // creates the table, dropping it first if it already existed
};

function waitFor(time = 1000) {
    return new Promise(resolve => setTimeout(resolve, time));
}

async function initDatabase({ sequelize, options }) {
    let connectionSuccess = false;
    while (!connectionSuccess) {
        await waitFor(2000);
        await sequelize.authenticate()
            .then(() => {
                connectionSuccess = true;
            })
            .catch((err) => {
                console.log(err);
            });
    }
    await sequelize.sync(options);
}


class DatabaseConnector {
    databases = [];

    init() {
        return Promise.all(this.databases.map(db => initDatabase(db)));
    }

    addDatabase(name, initType = InitType.PERSIST) {
        if (typeof name !== 'string') {
            console.warn('pass a name to add a database');
            return;
        }

        let sequelize = null;
        const databaseUrl = `postgres://${USER}:${PASSWORD}@db/${name}`;

        try {
            sequelize = new Sequelize(databaseUrl);
        } catch (err) {
            console.warn(`failed to connect to database with url ${databaseUrl}`);
            return;
        }

        const options = initOptions[initType];
        const db = { sequelize, options };

        this.databases.push(db);

        return db;
    }
}

module.exports = {
    Sequelize,
    InitType,
    databaseConnector: new DatabaseConnector()
};