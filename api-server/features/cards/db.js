const { Sequelize, InitType, databaseConnector } = require('../../lib/databaseConnector');

const db = databaseConnector.addDatabase('vokabeln', InitType.UPDATE);

db.User = db.sequelize.define('user', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    admin: Sequelize.BOOLEAN,
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    from: {
        type: Sequelize.JSON,
        allowNull: false
    },
    to: {
        type: Sequelize.JSON,
        allowNull: false
    }
    // add config or settings
});

// db.Deck = db.sequelize.define('deck', {
//     id: {
//         type: Sequelize.INTEGER,
//         autoIncrement: true,
//         primaryKey: true
//     },
//     userId: {
//         type: Sequelize.INTEGER,
//         allowNull: false
//     },
//     name: {
//         type: Sequelize.STRING,
//         allowNull: false
//     },
//     from: {
//         type: Sequelize.JSON,
//         allowNull: false
//     },
//     to: {
//         type: Sequelize.JSON,
//         allowNull: false
//     },
//     refreshMemoryStaleDate: {
//         type: Sequelize.DATE
//     },
//     refreshMemoryCardIds: {
//         type: Sequelize.JSON
//     },
//     settings: {
//         type: Sequelize.JSON
//     },
// });

db.Card = db.sequelize.define('card', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    user: {
        type: Sequelize.STRING,
        allowNull: false
    },
    translations: Sequelize.JSON,
    example: Sequelize.STRING,
    priority: Sequelize.STRING,
    lastSeenAt: Sequelize.BIGINT
});

db.getUser = function(name) {
    if (!name) {
        return this.User.findAll();
    }
    return this.User.findAll({
        where: { name }
    });
}

db.addUser = function(request) {
    const {
        name,
        from,
        to
    } = request;
    return this.User.create({
        name,
        from,
        to
    });
}

db.deleteUser = function(name) {
    return this.User.destroy({
        where: { name }
    });
}

db.getCards = function(user, id) {
    if (!user) {
        return this.Card.findAll();
    }
    if (!id) {
        return this.Card.findAll({
            where: { user }
        });
    }

    return this.Card.findAll({
        where: { user, id }
    });
}

db.deleteCard = function(id) {
    return this.Card.destroy({
        where: { id }
    })
}

db.addCard = function(request) {
    const {
        user,
        translations,
        example,
        priority,
        lastSeenAt
    } = request;
    return this.Card.create({
        user,
        translations,
        example,
        priority,
        lastSeenAt
    });
}

db.updateCard = function(request) {
    return this.Card.update(request, {
        where: {
            id: request.id
        }
    });
}

module.exports = db;