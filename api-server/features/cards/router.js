const db = require('./db');
const { isAdmin } = require('../../lib/middleware/isAdmin');


function router(app, express) {
    const router = express.Router();

    router.get('/all/:username', async (req, res) => {
        const cards = await db.getCards(req.params.username);
        res.json(cards);
    });

    router.get('/delete/:id', async (req, res) => {
        const id = req.params.id;
        if (!id) {
            res.json({
                success: false
            });
            return;
        }
        await db.deleteCard(id);
        res.json({
            success: true,
            id
        });
    });

    router.post('/upsert', async (req, res) => {
        let card;
        if (req.body.id) {
            await db.updateCard(req.body);
            const result = await db.getCards(req.body.user, req.body.id);
            card = result[0];
        } else {
            card = await db.addCard(req.body);
        }
        res.json(card);
    });

    router.get('/user/:name', async (req, res) => {
        const user = await db.getUser(req.params.name);
        if (!user) {
            res.json({
                success: false
            });
            return;
        }
        res.json({
            success: true,
            user: user[0]
        });
    });

    router.get('/all_users', async (req, res) => {
        const users = await db.getUser();
        res.json({
            success: true,
            users
        });
    });

    router.post('/upsert_user', isAdmin, app.oauth.authorise(), async (req, res) => {
        if (!req.body.name) {
            res.json({
                success: false,
                error: 'unkown user name'
            });
            return;
        }
        const user = await db.addUser(req.body);
        res.json({
            success: true,
            user
        });
    });

    router.post('/delete_user', isAdmin, app.oauth.authorise(), async (req, res) => {
        if (!req.body.name) {
            res.json({
                success: false,
                error: 'unkown user name'
            });
            return;
        }
        const id = await db.deleteUser(req.body.name);
        res.json({
            success: true,
            id
        });
    });

    return router;
}

module.exports = router;