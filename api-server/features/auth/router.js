const oAuth2Server = require('node-oauth2-server');
const tokenService = require('./tokenService');
const authenticator = require('./authenticator');
const { isAdmin } = require('../../lib/middleware/isAdmin');


function oAuth2Router(app, express) {
    app.oauth = oAuth2Server({
        model: tokenService,
        grants: [ 'password' ],
        debug: true
    });

    app.use(app.oauth.errorHandler());

    const router = express.Router();

    router.post('/get_all', isAdmin, authenticator.getAllUsers);
    router.post('/register', isAdmin, authenticator.registerUser);
    router.post('/delete', isAdmin, authenticator.deleteUser);
    router.post('/login', app.oauth.grant(), authenticator.login);

    return router;
}

module.exports = oAuth2Router;