const db = require('./db');


function sendResponse(res, message, error) {
    const success = error === undefined;
    res.status(!success ? 400 : 200).json({
        success,
        message,
        error
    });
}

function getAllUsers(req, res) {
    db.getAllUsers()
        .then(users => {
            res.json({
                success: true,
                users
            });
        })
        .catch(err => {
            sendResponse(res, 'something went wrong', err);
        });
}

function registerUser(req, res) {
    if (!req.body.username) {
        sendResponse(res, 'unkown username', true);
        return;
    }
    db.isValidUser(req.body.username)
        .then(result => {
            const isValidUser = result.length === 0;
            if (!isValidUser) {
                sendResponse(res, 'user already exists');
                return;
            }

            return db.register(req.body.username, req.body.password);
        })
        .then(() => {
            sendResponse(res, `registered ${req.body.username}`);
        })
        .catch(err => {
            sendResponse(res, 'something went wrong', err);
        });
}

function deleteUser(req, res) {
    if (!req.body.username) {
        sendResponse(res, 'unkown username', true);
        return;
    }
    db.deleteUser(req.body.username)
        .then(() => {
            sendResponse(res, `deleted ${req.body.username}`);
        })
        .catch(err => {
            sendResponse(res, 'something went wrong', err);
        });
}

function login(req, res) {}

module.exports = {
    getAllUsers,
    registerUser,
    deleteUser,
    login
};