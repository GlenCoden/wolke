const crypto = require('crypto');
const { Sequelize, InitType, databaseConnector } = require('../../lib/databaseConnector');

const db = databaseConnector.addDatabase('oauth2', InitType.PERSIST);

function getShaPass(password) {
    return crypto.createHash('sha256').update(password).digest('hex');
}


db.Users = db.sequelize.define('users', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    username: Sequelize.TEXT,
    user_password: Sequelize.TEXT
});

db.AccessTokens = db.sequelize.define('access_tokens', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    access_token: Sequelize.TEXT,
    user_id: Sequelize.INTEGER
});

db.register = function(username, password) {
    return this.Users.create({
        username,
        user_password: getShaPass(password)
    });
};

db.deleteUser = function(username) {
    return this.Users.destroy({
        where: { username }
    });
};

db.getUser = function(username, password) {
    const shaPass = getShaPass(password);
    return this.Users.findAll({
        where: {
            username,
            user_password: shaPass
        }
    });
};

db.getAllUsers = function() {
    return this.Users.findAll();
}

db.isValidUser = function(username) {
    return this.Users.findAll({
        where: {
            username
        }
    });
};

db.saveAccessToken = function(accessToken, userID) {
    return this.AccessTokens.create({
        access_token: accessToken,
        user_id: userID
    });
};

db.getUserIDFromBearerToken = function(bearerToken) {
    return this.AccessTokens.findAll({
        where: {
            access_token: bearerToken
        }
    });
};

module.exports = db;