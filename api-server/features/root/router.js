const sendMail = require('../../lib/smtpMailer');

function rootRouter(app, express) {
    const router = express.Router();

    router.post('/mail', async (req, res) => {
        console.log('POST MAIL - HOSTNAME', req.hostname); // TODO remove dev code
        const { from, to, subject, text } = req.body;
        if (!from || !to || !subject || !text) {
            res.sendStatus(400);
            return;
        }
        const response = await sendMail({ from, to, subject, text });
        res.json(response);
    });

    return router;
}

module.exports = rootRouter;
