const { Sequelize, InitType, databaseConnector } = require('../../lib/databaseConnector');

const db = databaseConnector.addDatabase('tsc', InitType.PERSIST);

db.Event = db.sequelize.define('event', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    name: Sequelize.STRING,
    date: Sequelize.STRING,
    place: Sequelize.STRING,
    host: Sequelize.STRING,
    start: Sequelize.STRING,
    final: Sequelize.BOOLEAN,
    disciplines: Sequelize.JSON,
    gymnastics: Sequelize.JSON,
    competitorIds: Sequelize.JSON,
    deleted: Sequelize.DATE
});

db.Competitor = db.sequelize.define('competitor', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    name: Sequelize.STRING,
    gender: {
        type: Sequelize.STRING,
        allowNull: false
    },
    year: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    weight: {
        type: Sequelize.JSON
    },
    club: Sequelize.STRING,
    results: Sequelize.JSON,
    deleted: Sequelize.DATE
});

// get all

db.getAll = function() {
    return Promise.all([
        this.Event.findAll(),
        this.Competitor.findAll()
    ]);
}

// events

db.getEvents = function(id) {
    if (!id) {
        return this.Event.findAll({
            where: { deleted: null }
        });
    }
    return this.Event.findAll({
        where: {
            [Sequelize.Op.and]: [
                { id },
                { deleted: null }
            ]
        }
    });
};

db.addEvent = function(request) {
    const {
        name,
        date,
        place,
        host,
        start,
        disciplines,
        gymnastics,
        competitorIds
    } = request;
    return this.Event.create({
        name,
        date,
        place,
        host,
        start,
        disciplines,
        gymnastics,
        competitorIds
    });
};

db.updateEvent = function(request) {
    return this.Event.update(request, {
        where: {
            id: request.id
        }
    });
};

db.deleteEvent = function(id) {
    return this.Event.update(
        {
            deleted: new Date()
        },
        {
            where: { id }
        }
    );
};

db.recoverEvent = function(id) {
    return this.Event.update(
        {
            deleted: null
        },
        {
            where: { id }
        }
    );
}

// competitors

db.getCompetitors = function(id) {
    if (!id) {
        return this.Competitor.findAll({
            where: { deleted: null }
        });
    }
    return this.Competitor.findAll({
        where: {
            [Sequelize.Op.and]: [
                { id },
                { deleted: null }
            ]
        }
    });
};

db.addCompetitor = function(request) {
    const {
        id,
        name,
        gender,
        year,
        weight,
        club,
        results
    } = request;
    return this.Competitor.create({
        id,
        name,
        gender,
        year,
        weight,
        club,
        results
    });
};

db.updateCompetitor = function(request) {
    return this.Competitor.update(request, {
        where: {
            id: request.id
        }
    });
};

db.deleteCompetitor = function(id) {
    return this.Competitor.update(
        {
            deleted: new Date()
        },
        {
            where: { id }
        }
    );
};

db.recoverCompetitor = function(id) {
    return this.Competitor.update(
        {
            deleted: null
        },
        {
            where: { id }
        }
    );
}

// clear deleted entities older than cacheTime

const cacheTime = 90; // days

db.clearCache = function() {
    const staleDate = new Date();
    staleDate.setMinutes(staleDate.getMinutes() - 60 * 24 * cacheTime);

    const deletedEventIds = this.Event.findAll({
        where: { deleted: { [Sequelize.Op.not]: null } }
    })
        .then(deletedEvents => deletedEvents.map(event => {
            if (new Date(event.deleted) < staleDate) {
                return this.Event.destroy({
                    where: { id: event.id }
                });
            }
            return Promise.resolve();
        }))
        .catch(console.error);

    const deletedCompetitorIds = this.Competitor.findAll({
        where: { deleted: { [Sequelize.Op.not]: null } }
    })
        .then(deletedCompetitors => deletedCompetitors.map(competitor => {
            if (new Date(competitor.deleted) < staleDate) {
                return this.Event.destroy({
                    where: { id: competitor.id }
                });
            }
            return Promise.resolve();
        }))
        .catch(console.error);

    return Promise.all([
        deletedEventIds,
        deletedCompetitorIds
    ]);
}

module.exports = db;
