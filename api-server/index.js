const express = require('express');
const app = express();
const { databaseConnector } = require('./lib/databaseConnector');
const oAuth2Router = require('./features/auth/router');
const rootRouter = require('./features/root/router');
const tscRouter = require('./features/tsc/router');
const cardsRouter = require('./features/cards/router');
const bertaRouter = require('./features/berta-berlin/router');

console.log('process.env', process.env);

const PORT = 5001;

const cors = require('cors');
app.use(cors());

const bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/auth', oAuth2Router(app, express));
app.use('/', rootRouter(app, express));
app.use('/tsc', tscRouter(app, express));
app.use('/cards', cardsRouter(app, express));
app.use('/berta', bertaRouter(app, express));

app.use(express.static(`${__dirname}/static`));

databaseConnector.init()
    .then(() => {
        app.listen(PORT, () => console.log(`Listening on port ${PORT}.`));
    })
    .catch(err => {
        console.error('cannot connect to database', err);
    });
